# Pré-requis:

Les binaires de `gdcm`sont requis par pydicom.

## Sous debian:
```bash
apt install libvtkgdcm-tools libgdcm2.8 python3-pysha3
```

**Note:** le module pysha3 ne s'installe pas facilement avec pip sous debian, on peut passer par le gestionnaire de paquet apt.

## Sous Mac OS:

```bash
brew install gdcm
```

**Note:** attention gdmc dépend de python3.9, donc bien remettre le python3.8 par défaut si c'est la version que vous utilisez habituellement. `brew link --overwrite python@3.8`.

# Installation

```
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/ljk-scoliose/collecte_data.git
pip install -r requirements.txt
```


# Utilisation

Lancer le notebook :
```
jupyter notebook
```

Lancer l'application `voila`.

```bash
voila --autoreload=1 Collecte_anonymisation_dicom.ipynb
```
